const jwt = require('jsonwebtoken')

const secret = "CourseBookingAPI"

module.exports.createAccessToken = (user) =>{
	const data = {
		id: user._id,
		emailL: user.email,
		isAdmin: user.isAdmin

	}
	return jwt.sign(data, secret,{})
	//(<payload><secretkey><{option}>)
}

module.exports.verify = (request,response,next) => {
	//console.log(request.headers)
	let token = request.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token)

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return response.send({auth: "failed"})
			}else{
				next()
			}
		})
	}else {
		return response.send({auth: "failed"})
	}
}

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){

		token = token.slice(7, token.length);

		return jwt.verify(token,secret, (err,data) => {

			if(err){
				return null
			}else{

				return jwt.decode(token,{complete: true}).payload
			}
		})
	}else{
		return null
	}
}



